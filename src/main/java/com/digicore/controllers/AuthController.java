package com.digicore.controllers;

import com.digicore.http.requests.CreateAccountRequest;
import com.digicore.http.requests.LoginRequest;
import com.digicore.http.response.CreateAccountResponse;
import com.digicore.http.response.LoginResponse;
import com.digicore.services.intefaces.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/v1")
@RequiredArgsConstructor
public @RestController class AuthController {
    private final AccountService accountService;

    @PostMapping("/login")
    public @ResponseBody LoginResponse doLogin(@RequestBody LoginRequest loginRequest){
        return accountService.doLogin(loginRequest);
    }

    @PostMapping("/create-account")
    public @ResponseBody CreateAccountResponse doRegistration(@RequestBody CreateAccountRequest createAccountRequest){
        return accountService.createAccount(createAccountRequest);
    }
}
