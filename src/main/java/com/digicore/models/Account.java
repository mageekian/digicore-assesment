package com.digicore.models;

import com.digicore.models.enums.ROLE;
import com.digicore.util.AccountNumberGenerator;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account implements UserDetails {
    private final String accountNumber = AccountNumberGenerator.generate();
    private final Boolean isAccountNonExpired = true;
    private final Boolean isAccountNonLocked = true;
    private final Boolean isCredentialsNonExpired = true;
    private final Boolean isEnabled = true;
    private String accountName;
    private String accountPassword;
    @Getter
    @Setter
    private Double balance = 0d;

    @Getter
    private final List<Transaction> transactions = new ArrayList<>();


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Role role = new Role();
        role.setRole(ROLE.ACCOUNT);
        return List.of(role);
    }

    @Override
    public String getPassword() {
        return accountPassword;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    @Override
    public String getUsername() {
        return accountNumber;
    }

    @Override
    public boolean isAccountNonExpired() {
        return isAccountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return isAccountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return isCredentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }
}
