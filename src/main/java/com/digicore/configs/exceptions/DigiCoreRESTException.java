package com.digicore.configs.exceptions;

public class DigiCoreRESTException extends RuntimeException{
    public DigiCoreRESTException() {
    }

    public DigiCoreRESTException(String message) {
        super(message);
    }

    public DigiCoreRESTException(String message, Throwable cause) {
        super(message, cause);
    }
}
