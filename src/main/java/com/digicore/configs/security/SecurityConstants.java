package com.digicore.configs.security;

public final class SecurityConstants {

    public static final String[] PUBLIC_URIS= new String[]{
            "/v1/login",
            "/v1/create-account",
            "/"
    };
}
