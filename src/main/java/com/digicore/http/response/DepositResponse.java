package com.digicore.http.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;



@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepositResponse implements Serializable {
    private int responseCode;
    private boolean success;
    private String message;
}
