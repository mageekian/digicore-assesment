package com.digicore.http.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountSummaryResponse {
    private String accountName;
    private String accountNumber;
    private Double balance;
}
