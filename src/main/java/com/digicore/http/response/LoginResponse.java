package com.digicore.http.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponse implements Serializable{

        private String accessToken;
        private boolean success;

}
