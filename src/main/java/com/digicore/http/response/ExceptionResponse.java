package com.digicore.http.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.time.LocalDate;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class ExceptionResponse implements Serializable {
    private String message;
    private HttpStatus httpStatus;
    private LocalDate timestamp;
}
