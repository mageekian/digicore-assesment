package com.digicore.http.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;



@Data
@NoArgsConstructor
@AllArgsConstructor
public class WithdrawRequest implements Serializable {
    private String accountNumber;
    private String accountPassword;
    private Double withdrawnAmount;
}
